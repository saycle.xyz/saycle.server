﻿using System;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using saycle.server.Data;
using saycle.server.Models;
using saycle.server.ViewModels.Story;

namespace saycle.server.Controllers
{
    /// <summary>
    /// Handles requests on the <see cref="Language"/> entity.
    /// </summary>
    [Route("api/[controller]")]
    public class LanguageController : BaseController
    {
        /// <summary>
        /// Initialize <see cref="Language"/> controller.
        /// </summary>
        public LanguageController(SaycleContext context, IMapper mapper) : base(context, mapper)
        {
        }

        /// <summary>
        /// Returns all <see cref="Language"/> entities.
        /// </summary>
        /// <returns>Language entities as JSON</returns>
        [HttpGet]
        public JsonResult Get()
        {
            return Json(Context.Language.ToList());
        }

        /// <summary>
        /// Returns the <see cref="Language"/> entity with the corresponding <see cref="Language.Code"/>.
        /// </summary>
        /// <param name="code">Identitfier of the language</param>
        /// <returns>Language entity as JSON</returns>
        [HttpGet("{code}")]
        public JsonResult Get(string code)
        {
            return Json(Context.Language.FirstOrDefault(s => Equals(s.Code, code)));
        }
    }
}
