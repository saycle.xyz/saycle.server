﻿using System;
using System.Data;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using saycle.server.Data;
using saycle.server.Models;
using saycle.server.ViewModels.Story;

namespace saycle.server.Controllers
{
    /// <summary>
    /// Handles requests on the system status.
    /// </summary>
    [Route("api/[controller]")]
    public class StatusController : BaseController
    {
        /// <summary>
        /// Initialize <see cref="Language"/> controller.
        /// </summary>
        public StatusController(SaycleContext context, IMapper mapper) : base(context, mapper)
        {
        }

        /// <summary>
        /// Returns if the database exists or could be created.
        /// </summary>
        /// <returns>If Database connection works</returns>
        [HttpGet]
        public bool CheckDatabase()
        {
            Context.Database.OpenConnection();
            return Context?.Database?.GetDbConnection().State == ConnectionState.Open;
        }
    }
}
