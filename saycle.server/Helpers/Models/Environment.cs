﻿namespace saycle.server.Helpers.Models
{
    /// <summary>
    /// Environments for development, testing and production.
    /// </summary>
    public enum Environment
    {
        /// <summary>
        /// Development environment.
        /// </summary>
        D,
        /// <summary>
        /// Quality Assurance/Testing environment.
        /// </summary>
        Q,
        /// <summary>
        /// Production environment.
        /// </summary>
        P
    }
}
