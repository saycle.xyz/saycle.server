# /bin/sh

cd saycle-server
dotnet clean
echo "build for ubuntu.16.04-x64"
dotnet build -c release -r ubuntu.16.04-x64
echo "build for ubuntu.16.10-x64"
dotnet build -c release -r ubuntu.16.10-x64
# echo "build for ubuntu.17.04-x64"
# dotnet build -c release -r ubuntu.17.04-x64
# echo "build for ubuntu.17.10-x64"
# dotnet build -c release -r ubuntu.17.10-x64
